import {
  Tree,
  SchematicContext,
  Rule,
  mergeWith,
  apply,
  url,
  template,
} from "@angular-devkit/schematics";
import { strings } from "@angular-devkit/core";

import { getProjectName } from "../util";

export default function (_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const project = getProjectName(tree, _options);

    const parametrizedTemplate = apply(url("./files"), [
      template({ ...strings, project }),
    ]);

    return mergeWith(parametrizedTemplate);
  };
}
