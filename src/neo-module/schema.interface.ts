import { ModuleType } from "../models";

export interface Schema {
  name: string;
  type?: ModuleType;
  project?: string;
}
