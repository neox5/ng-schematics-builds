import {
  Rule,
  SchematicContext,
  Tree,
  url,
  apply,
  template,
  mergeWith,
  move,
  chain,
  schematic,
} from "@angular-devkit/schematics";
import { strings } from "@angular-devkit/core";

// util
import { parsePathAndPrefix } from "../util";

import { Schema } from "./schema.interface";
import { ModuleType } from "../models";

export default function (_options: Schema): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const { name, path, prefix } = parsePathAndPrefix(tree, _options);

    const templatesUrl = getTemplateUrl(_options.type);
    const sourceTemplates = url(templatesUrl);
    const sourceParametrizedTemplates = apply(sourceTemplates, [
      template({
        ..._options,
        ...strings,
        prefix,
        name, // overwrite _options.name value
      }),
      move(path),
    ]);

    let rule = mergeWith(sourceParametrizedTemplates);

    // additional file generation
    switch (_options.type) {
      case "core":
        break;
      case "layout":
        const layoutViewComponentOptions = {
          name: "layout/views/layout-view",
          template: layoutViewComponentTemplate(prefix),
          project: _options.project,
        };
        const navigationComponentOptions = {
          name: "layout/containers/navigation",
          project: _options.project,
        };

        rule = chain([
          rule,
          schematic("nc", layoutViewComponentOptions),
          schematic("nc", navigationComponentOptions),
        ]);
        break;
      case "routes":
        break;
      case "shared":
        break;
      default:
        const sandboxServiceOptions = {
          name: _options.name + "/services/" + name,
          type: "sandbox",
          project: _options.project,
        };

        const viewComponentOptions = {
          name: _options.name + "/views/" + name + "-view",
          project: _options.project,
        };

        rule = chain([
          rule,
          schematic("nc", viewComponentOptions),
          schematic("ns", sandboxServiceOptions),
        ]);
    }

    return rule(tree, _context);
  };
}

function getTemplateUrl(type?: ModuleType): string {
  switch (type) {
    case "core":
      return "./files/core";
    case "layout":
      return "./files/layout";
    case "routes":
      return "./files/routes";
    case "shared":
      return "./files/shared";
    default:
      return "./files/module";
  }
}

function layoutViewComponentTemplate(prefix: string): string {
  return `<${prefix}-navigation></${prefix}-navigation>\n<router-outlet></router-outlet>`;
}
