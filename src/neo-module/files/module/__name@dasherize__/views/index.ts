export const views: any[] = [];

export * from "./<%= dasherize(name) %>-view/<%= dasherize(name) %>-view.component";
