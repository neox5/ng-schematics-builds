import {
  Rule,
  SchematicContext,
  Tree,
  chain,
  schematic,
  apply,
  url,
  move,
  mergeWith,
  template,
} from "@angular-devkit/schematics";

import { Schema } from "./schema.interface";
import { ModuleType } from "../models";
import { getPrefix } from "../util";
import { strings } from "@angular-devkit/core";

export default function (_options: Schema): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const prefix = getPrefix(tree, _options);

    let rule = chain([
      schematic("nm", moduleOptions("core", _options.project)),
      schematic("nm", moduleOptions("layout", _options.project)),
      schematic("nm", moduleOptions("routes", _options.project)),
      schematic("nm", moduleOptions("shared", _options.project)),
    ]);

    // update app.module
    tree.delete("./src/app/app.module.ts");
    tree.delete("./src/app/app.component.ts");
    if (tree.exists("./src/app/app.component.html")) {
      tree.delete("./src/app/app.component.html");
    }
    const appModule = apply(url("./files/app"), [
      template({
        ...strings,
        prefix,
      }),
      move("./src/app"),
    ]);
    rule = chain([rule, mergeWith(appModule)]);

    return rule(tree, _context);
  };
}

function moduleOptions(type: ModuleType, project: string | undefined) {
  return {
    name: type,
    type: type,
    project,
  };
}
