import {
  Tree,
  SchematicContext,
  Rule,
  mergeWith,
  url,
} from "@angular-devkit/schematics";

import { getConfig } from "../util";

export default function (_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const packageJson = getConfig(tree, "package.json");

    const stylelintScript =
      "npm install --save-dev stylelint " +
      "stylelint-config-recommended-scss " +
      "stylelint-config-standard " +
      "stylelint-order " +
      "stylelint-prettier " +
      "prettier " +
      "stylelint-scss";

    packageJson.scripts["add-stylelint"] = stylelintScript;
    packageJson.scripts["stylelint-scss"] = "npx stylelint src/**/*.scss";
    tree.overwrite("package.json", JSON.stringify(packageJson, null, 2));

    // add .stylelintrc
    return mergeWith(url("./files"));
  };
}
