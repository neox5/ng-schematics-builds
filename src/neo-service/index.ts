import {
  Rule,
  SchematicContext,
  Tree,
  url,
  apply,
  template,
  mergeWith,
  move,
  chain,
  schematic,
} from "@angular-devkit/schematics";
import { strings } from "@angular-devkit/core";

// util
import { parsePathAndPrefix } from "../util";

import { Schema } from "./schema.interface";

export default function (_options: Schema): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const { name, path, prefix } = parsePathAndPrefix(tree, _options);

    let templatesUrl: string = "";

    switch (_options.type) {
      case "api":
        templatesUrl = "./files/api";
        break;
      case "sandbox":
        templatesUrl = "./files/sandbox";
        break;
      case "util":
        templatesUrl = "./files/util";
        break;
      default:
        templatesUrl = "./files/service";
    }

    const sourceTemplates = url(templatesUrl);
    const sourceParametrizedTemplates = apply(sourceTemplates, [
      template({
        ..._options,
        ...strings,
        prefix,
        name, // overwrite _options.name value
      }),
      move(path),
    ]);

    let rule = mergeWith(sourceParametrizedTemplates);

    if (_options.type === "sandbox") {
      const utilServiceOptions = {
        name: _options.name,
        type: "util",
        project: _options.project,
      };

      rule = chain([rule, schematic("ns", utilServiceOptions)]);

      // create index.ts exporting util service
      const indexPath = path + "/util/index.ts";
      const utilExport = `export * from "./${name}/${name}-util.service";`;
      tree.create(indexPath, utilExport);
    }

    return rule(tree, _context);
  };
}
