import { ServiceType } from "../models";

export interface Schema {
  name: string;
  type?: ServiceType;
  project?: string;
}
