import {
  Rule,
  SchematicContext,
  Tree,
  url,
  template,
  mergeWith,
  apply,
  chain,
  schematic,
} from "@angular-devkit/schematics";

import { Schema as InitOptions } from "./schema.interface";
import { getConfig, getProjectName } from "../util";

export default function (_options: InitOptions): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const workspace = getConfig(tree, "angular.json");
    const tslint = getConfig(tree, "tslint.json");
    const tsconfig = getConfig(tree, "tsconfig.json");
    const packageJson = getConfig(tree, "package.json");

    // modify angular.json
    const pn = getProjectName(tree, _options);
    if (_options.prefix) {
      workspace["projects"][pn].prefix = _options.prefix;
    }
    workspace["projects"][pn]["schematics"]["@schematics/angular:component"] = {
      inlineTemplate: true,
      style: "scss",
    };

    const opt = workspace["projects"][pn]["architect"]["build"]["options"];
    workspace["projects"][pn]["architect"]["build"]["options"] = {
      ...opt,
      stylePreprocessorOptions: {
        includePaths: ["src/styles/scss"],
      },
    };

    // modify tslint.json
    tslint.rules.quotemark = [true, _options.quotes];

    let current = tslint["rules"]["no-inferrable-types"];
    if (current.indexOf("no-inferrable-types") === -1) {
      tslint["rules"]["no-inferrable-types"] = [
        ...current,
        "ignore-properties",
      ];
    }

    current = tslint["rules"]["variable-name"]["options"];
    if (current.indexOf("allow-leading-underscore") === -1) {
      tslint["rules"]["variable-name"]["options"] = [
        ...current,
        "allow-leading-underscore",
      ];
    }

    // modify tsconfig.base.json
    tsconfig.compilerOptions.baseUrl = "./src";
    tsconfig.compilerOptions["paths"] = {};
    tsconfig.compilerOptions["paths"]["@assets/*"] = ["assets/*"];
    tsconfig.compilerOptions["paths"]["@core/*"] = ["app/core/*"];
    tsconfig.compilerOptions["paths"]["@layout/*"] = ["app/layout/*"];
    tsconfig.compilerOptions["paths"]["@routes/*"] = ["app/routes/*"];
    tsconfig.compilerOptions["paths"]["@shared/*"] = ["app/shared/*"];
    tsconfig.compilerOptions["paths"]["@env/*"] = ["environments/*"];
    
    // adds prod script to package.json
    packageJson.scripts["prod"] = "ng build --prod";

    // overwrite config files
    tree.overwrite("angular.json", JSON.stringify(workspace, null, 2));
    tree.overwrite("tslint.json", JSON.stringify(tslint, null, 2));
    tree.overwrite("tsconfig.json", JSON.stringify(tsconfig, null, 2));
    tree.overwrite("package.json", JSON.stringify(packageJson, null, 2));

    // modify editorconfig
    tree.delete(".editorconfig"); // workaround
    const parametrizedConfig = apply(url("./files/editorconfig"), [
      template({ quotes: _options.quotes }),
    ]);
    let rule = mergeWith(parametrizedConfig);

    // add neo styles
    if (_options.styles) {
      rule = chain([rule, schematic("nst", _options)]);
    }

    // add neo stylelint
    if (_options.stylelint) {
      rule = chain([rule, schematic("nslint", _options)]);
    }

    return rule(tree, _context);
  };
}
