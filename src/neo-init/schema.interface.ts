export interface Schema {
  prefix: string;
  quotes: string;
  styles: boolean;
  stylelint: boolean;
  project?: string;
}
