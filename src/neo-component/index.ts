import {
  Rule,
  SchematicContext,
  Tree,
  url,
  apply,
  template,
  mergeWith,
  move,
  SchematicsException,
} from "@angular-devkit/schematics";
import { strings, join, Path } from "@angular-devkit/core";
import { tsquery } from "@phenomnomnominal/tsquery";

import * as ts from "typescript";

// util
import { parsePathAndPrefix, readIntoSourceFile } from "../util";

import { Schema as ComponentOptions } from "./schema.interface";
import { InsertChange } from "@schematics/angular/utility/change";
import { getSourceNodes } from "@schematics/angular/utility/ast-utils";
import { getQuoteSetting } from "../util/get-quote-setting";

export default function(_options: ComponentOptions): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const componentOptions = _options;
    const { name, path, prefix } = parsePathAndPrefix(tree, _options);

    componentOptions.name = name;
    componentOptions.path = path;
    componentOptions.prefix = prefix;
    componentOptions.quotes = getQuoteSetting(tree);

    // add template
    const sourceTemplates = url("./files");
    const sourceParametrizedTemplates = apply(sourceTemplates, [
      template({ ...componentOptions, ...strings }),
      move(componentOptions.path),
    ]);

    // add import to index.ts
    addImportToIndex(tree, componentOptions);

    return mergeWith(sourceParametrizedTemplates)(tree, _context);
  };
}

function addImportToIndex(tree: Tree, options: ComponentOptions) {
  if (options.skipImport) {
    return;
  }

  const name = options.name;
  const indexPath = findIndexFromOptions(tree, options);

  if (!indexPath) {
    throw new SchematicsException(
      "Could not found indexPath for adding import"
    );
  }

  let sourceFile = readIntoSourceFile(tree, indexPath);

  // add component import
  const importNodes = getSourceNodes(sourceFile).filter(
    (n) => n.kind === ts.SyntaxKind.ImportDeclaration
  );

  let pos;
  let addNewLine = false;
  if (importNodes.length === 0) {
    pos = tsquery(sourceFile, "VariableStatement")[0].getStart() - 1;
    addNewLine = true;
  } else {
    pos = importNodes[importNodes.length - 1].getEnd();
  }
  let importStr =
    `import { ${strings.classify(name)}Component } ` +
    "from " +
    `"./${strings.dasherize(name)}/${strings.dasherize(name)}.component";\n`;


  if (options.quotes === "single") {
    importStr = importStr.replace(/"/g, "'");
  }

  if (addNewLine) {
    importStr += "\n";
  }

  let change = new InsertChange(indexPath, pos + 1, importStr);

  let recorder = tree.beginUpdate(indexPath);
  recorder.insertLeft(change.pos, change.toAdd);
  tree.commitUpdate(recorder);

  // add Component to export array
  sourceFile = readIntoSourceFile(tree, indexPath);
  importStr = strings.classify(name) + "Component";
  const identifierCount = tsquery(
    sourceFile,
    "ArrayLiteralExpression > Identifier"
  ).length;
  pos = tsquery(sourceFile, "ArrayLiteralExpression")[0].getEnd();

  if (identifierCount !== 0) {
    importStr = ", " + importStr;
  }

  change = new InsertChange(indexPath, pos - 1, importStr);

  recorder = tree.beginUpdate(indexPath);
  recorder.insertLeft(change.pos, change.toAdd);
  tree.commitUpdate(recorder);
}

function findIndexFromOptions(
  tree: Tree,
  options: ComponentOptions
): string | undefined {
  let path = options.path as Path;

  if (!path) {
    return undefined;
  }

  path = join(path, "index.ts");

  return tree.exists(path) ? path : undefined;
}
