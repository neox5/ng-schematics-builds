import { ComponentType } from "../models";

export interface Schema {
  name: string;
  type: ComponentType;
  skipImport: boolean;
  path?: string;
  prefix?: string;
  quotes?: "single" | "double",
  template?: string;
  sandboxName?: string;
  destroyable?: boolean;
  forms?: boolean;
  renderer?: boolean;
  router?: boolean;
  project?: string;
}
