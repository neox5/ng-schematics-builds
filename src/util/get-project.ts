import { Tree, SchematicsException } from "@angular-devkit/schematics";
import { getProjectName } from "./get-project-name";

export function getProject(tree: Tree, _options: { project?: string }): any {
  const workspaceConfigBuffer = tree.read("angular.json");
  if (!workspaceConfigBuffer) {
    throw new SchematicsException(
      "Not an Angular CLI workspace: missing angular.json"
    );
  }

  const workspaceConfig = JSON.parse(workspaceConfigBuffer.toString());
  const projectName = getProjectName(tree, _options);
  return workspaceConfig.projects[projectName];
}
