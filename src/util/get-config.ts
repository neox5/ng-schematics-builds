import { Tree, SchematicsException } from "@angular-devkit/schematics";
import * as jsonStripComments from "strip-json-comments";

export function getConfig(tree: Tree, configPath: string): any {
  const configBuffer = tree.read(configPath);
  if (configBuffer === null) {
    throw new SchematicsException(`Could not find ${configPath}`);
  }

  const config = configBuffer.toString();
  return JSON.parse(jsonStripComments(config));
}
