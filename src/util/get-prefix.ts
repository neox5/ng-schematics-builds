import { Tree } from "@angular-devkit/schematics";
import { getProject } from "./get-project";

export function getPrefix(tree: Tree, options: { project?: string }): string {
  const project = getProject(tree, options);
  const prefix = project && project["prefix"] ? project["prefix"] : "app";
  
  return prefix;
}