import { Tree, SchematicsException } from "@angular-devkit/schematics";

export function getProjectName(
  tree: Tree,
  _options: { project?: string }
): string {
  const workspaceConfigBuffer = tree.read("angular.json");
  if (!workspaceConfigBuffer) {
    throw new SchematicsException(
      "Not an Angular CLI workspace: missing angular.json"
    );
  }

  const workspaceConfig = JSON.parse(workspaceConfigBuffer.toString());
  return _options.project || workspaceConfig.defaultProject;
}
