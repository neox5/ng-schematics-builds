import { Tree } from "@angular-devkit/schematics";
import { buildDefaultPath } from "@schematics/angular/utility/project";
import { parseName } from "@schematics/angular/utility/parse-name";
import { getProject } from "./get-project";

export function parsePathAndPrefix(
  tree: Tree,
  _options: { name: string; project?: string }
): { name: string; path: string; prefix: string } {
  const project = getProject(tree, _options);
  const prefix = project["prefix"] ? project["prefix"] : "app";

  const defaultProjectPath = buildDefaultPath(project);
  const parsedPath = parseName(defaultProjectPath, _options.name);

  const { name, path } = parsedPath;

  return { name, path, prefix };
}
