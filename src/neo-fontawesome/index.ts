import {
  Tree,
  SchematicContext,
  apply,
  url,
  template,
  mergeWith,
  Rule,
} from "@angular-devkit/schematics";
import { strings } from "@angular-devkit/core";

import {
  addImportToModule,
  addExportToModule,
} from "@schematics/angular/utility/ast-utils";
import { InsertChange } from "@schematics/angular/utility/change";

import { getConfig, readIntoSourceFile } from "../util";

import { Schema } from "./schema.interface";

export default function (_options: Schema): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    let rule: Rule = (host: Tree, _context: SchematicContext) => host;

    const packageJson = getConfig(tree, "package.json");
    const token = _options.token;

    let fontawesomeScript =
      "yarn add @fortawesome/angular-fontawesome @fortawesome/fontawesome-svg-core";
    if (_options.solid) {
      fontawesomeScript += " @fortawesome/";
      fontawesomeScript += token !== "" ? "pro" : "free";
      fontawesomeScript += "-solid-svg-icons";
    }
    if (_options.regular) {
      fontawesomeScript += " @fortawesome/";
      fontawesomeScript += token !== "" ? "pro" : "free";
      fontawesomeScript += "-regular-svg-icons";
    }
    if (_options.brands) {
      fontawesomeScript += " @fortawesome/";
      fontawesomeScript += "free-brands-svg-icons";
    }
    if (_options.light && token !== "") {
      fontawesomeScript += " @fortawesome/pro-light-svg-icons";
    }
    if (_options.duotone && token !== "") {
      fontawesomeScript += " @fortawesome/pro-duotone-svg-icons";
    }

    packageJson.scripts["add-fontawesome"] = fontawesomeScript;
    tree.overwrite("package.json", JSON.stringify(packageJson, null, 2));

    if (!_options.skipImport) {
      const modulePath = "src/app/shared/shared.module.ts";
      const importName = "FontAwesomeModule";
      const importPath = "@fortawesome/angular-fontawesome";

      let source = readIntoSourceFile(tree, modulePath);
      // import
      const importChanges = addImportToModule(
        source,
        modulePath,
        importName,
        importPath
      );

      const importRecorder = tree.beginUpdate(modulePath);
      for (const change of importChanges) {
        if (change instanceof InsertChange) {
          importRecorder.insertLeft(change.pos, change.toAdd);
        }
      }
      tree.commitUpdate(importRecorder);

      // export
      // Need to refresh the AST because we overwrote the file in the tree.
      source = readIntoSourceFile(tree, modulePath);
      const exportChanges = addExportToModule(
        source,
        modulePath,
        importName,
        importPath
      );

      const exportRecorder = tree.beginUpdate(modulePath);
      for (const change of exportChanges) {
        if (change instanceof InsertChange) {
          exportRecorder.insertLeft(change.pos, change.toAdd);
        }
      }
      tree.commitUpdate(exportRecorder);
    }

    // add .npmrc
    if (_options.token !== "") {
      console.log("...add .npmrc");
      const token = _options.token;
      const parametrizedTemplate = apply(url("./files"), [
        template({ ...strings, token }),
      ]);

      return mergeWith(parametrizedTemplate);
    }

    return rule(tree, _context);
  };
}
