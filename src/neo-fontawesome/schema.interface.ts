export interface Schema {
  token: string;
  solid: boolean;
  regular: boolean;
  brands: boolean;
  light: boolean;
  duotone: boolean;
  skipImport: boolean;
  project: string;
}
