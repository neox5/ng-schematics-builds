import {
  Rule,
  SchematicContext,
  Tree,
  url,
  mergeWith,
  apply,
  move,
  chain,
} from "@angular-devkit/schematics";

import { Schema } from "./schema.interface";

export default function (_options: Schema): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    tree.delete("./src/styles.scss"); // workaround
    const styleTemplate = apply(url("./files/main"), [move("./src")]);

    let rule = mergeWith(styleTemplate);

    const stylesFolder = apply(url("./files/sub"), [move("./src")]);
    rule = chain([rule, mergeWith(stylesFolder)]);

    return rule(tree, _context);
  };
}
