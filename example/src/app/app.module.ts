import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { CoreModule } from "@core/core.module";
import { LayoutModule } from "@layout/layout.module";
import { RoutesModule } from "@routes/routes.module";

import { AppComponent } from "./app.component";

import "@angular/common/locales/global/de-AT";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, CoreModule, LayoutModule, RoutesModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
