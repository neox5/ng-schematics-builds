import { LayoutViewComponent } from "./layout-view/layout-view.component";

export const views: any[] = [LayoutViewComponent];

export * from "./layout-view/layout-view.component";
