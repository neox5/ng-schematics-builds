import { Routes } from "@angular/router";

// layout
import { LayoutViewComponent } from "@layout/views";

// views
import { UserViewComponent } from "./user/views";

// =============================================================================
//
// ROUTES
//
// =============================================================================
export const ROUTES: Routes = [
  {
    path: "",
    pathMatch: "prefix",
    component: LayoutViewComponent,
    children: [
      {
        path: "",
        pathMatch: "full",
        component: UserViewComponent,
      },
    ],
  },
];
