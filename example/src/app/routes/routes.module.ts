import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { ROUTES } from "./routes";

// modules
import { UserModule } from "./user/user.module";

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(ROUTES, { enableTracing: false }),
    // modules
    UserModule
  ],
  exports: [RouterModule],
})
export class RoutesModule {}
