import { TestBed } from '@angular/core/testing';

import { UserSandboxService } from './user-sandbox.service';

describe('UserSandboxService', () => {
  let service: UserSandboxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserSandboxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});